import { ActionTypes } from "actions/ActionTypes";
import { boardReducer, mockTasks } from "reducers/boardReducer";

describe("Board reducer", () => {
  let initialState = {
    tasks: mockTasks
  };

  it("should return the initial state", () => {
    expect(boardReducer(undefined, {})).toEqual(initialState);
  });

  it("should handle CREATE_TASK", () => {
    const newTask = {
      id: "00000000-0000-0000-0000-000000000000",
      title: "new title",
      status: "done"
    };
    expect(
      boardReducer(initialState, {
        type: ActionTypes.CREATE_TASK,
        newTask
      })
    ).toEqual({
      ...initialState,
      tasks: [...mockTasks, newTask]
    });
  });

  it("should handle MOVE_TASK", () => {
    let expectedTasks = [...mockTasks];
    expectedTasks[0].status = "done";
    expect(
      boardReducer(initialState, {
        type: ActionTypes.MOVE_TASK,
        id: "123",
        status: "done"
      })
    ).toEqual({
      ...initialState,
      tasks: [...expectedTasks]
    });
  });

  it("should handle EDIT_TASK", () => {
    let expectedTasks = [...mockTasks];
    expectedTasks[0].title = "new title";
    expect(
      boardReducer(initialState, {
        type: ActionTypes.EDIT_TASK,
        id: "123",
        title: "new titile"
      })
    ).toEqual({
      ...initialState,
      tasks: [...expectedTasks]
    });
  });

  it("should handle DELETE_TASK", () => {
    let expectedTasks = [
      { id: "223", title: "Project planning", status: "in-progress" },
      { id: "323", title: "Run marathon", status: "done" },
      { id: "423", title: "Go to doctor", status: "to-do" },
      { id: "523", title: "Job interview", status: "to-do" },
      { id: "623", title: "Live happy", status: "in-progress" }
    ];
    expect(
      boardReducer(initialState, {
        type: ActionTypes.DELETE_TASK,
        id: "123"
      })
    ).toEqual({
      ...initialState,
      tasks: [...expectedTasks]
    });
  });

  it("should handle ADD_SEARCH_QUERY", () => {
    expect(
      boardReducer(initialState, {
        type: ActionTypes.ADD_SEARCH_QUERY,
        searchQuery: "new task"
      })
    ).toEqual({
      ...initialState,
      searchQuery: "new task"
    });
  });
});

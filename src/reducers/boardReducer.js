import { ActionTypes } from "actions/actionTypes";

export const mockTasks = [
  { id: "123", title: "Call Ben at 10", status: "to-do" },
  { id: "223", title: "Project planning", status: "in-progress" },
  { id: "323", title: "Run marathon", status: "done" },
  { id: "423", title: "Go to doctor", status: "to-do" },
  { id: "523", title: "Job interview", status: "to-do" },
  { id: "623", title: "Live happy", status: "in-progress" }
];

const defaultState = {
  tasks: mockTasks
};

export const boardReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ActionTypes.CREATE_TASK:
      return {
        ...state,
        tasks: [...state.tasks, action.newTask]
      };

    case ActionTypes.MOVE_TASK:
      const index = state.tasks.findIndex(task => task.id === action.id);
      let tasks = [...state.tasks];
      tasks[index].status = action.status;

      return {
        ...state,
        tasks: tasks
      };

    case ActionTypes.EDIT_TASK:
      const editIndex = state.tasks.findIndex(task => task.id === action.id);
      let updatedTasks = [...state.tasks];
      updatedTasks[editIndex].title = action.title;

      return {
        ...state,
        tasks: updatedTasks
      };

    case ActionTypes.DELETE_TASK:
      return {
        ...state,
        tasks: state.tasks.filter(task => task.id !== action.id)
      };

    case ActionTypes.ADD_SEARCH_QUERY:
      return {
        ...state,
        searchQuery: action.searchQuery
      };

    default:
      return state;
  }
};

import styled from "styled-components";
import { blue, lightBlue } from "style/colors";

export const StyledSearchForm = styled.form`
  text-align: right;
  margin-bottom: 10px;
  margin-right: 2px;
`;
StyledSearchForm.displayName = "StyledSearchForm";

export const StyledSearchInput = styled.input`
  height: 20px;
  padding: 2px 0px;
`;
StyledSearchInput.displayName = "StyledSearchInput";

export const StyledSearchButton = styled.input`
  padding: 5px 20px;
  background-color: ${lightBlue};
  border: 1px solid ${lightBlue};
  font-weight: bold;
  color: white;

  :hover {
    cursor: pointer;
    background-color: ${blue};
    border: 1px solid ${blue};
  }
`;
StyledSearchButton.displayName = "StyledSearchButton";

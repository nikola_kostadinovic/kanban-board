import React from "react";
import { mount } from "enzyme";
import SearchForm from "components/SearchForm/SearchForm";
import configureMockStore from "redux-mock-store";
import thunkMiddleware from "redux-thunk";
import { Provider } from "react-redux";
import { mockTasks } from "reducers/boardReducer";

const mockStore = configureMockStore([thunkMiddleware]);
const initialState = {
  boardReducer: {
    tasks: [...mockTasks]
  }
};
const store = mockStore(initialState);

const SearchFormProps = {
  searchQuery: "test"
};

function createMountSearchForm(props) {
  return mount(
    <Provider store={store}>
      <SearchForm {...SearchFormProps} {...props} />
    </Provider>
  );
}

describe("<SearchForm />", () => {
  let SearchFormComp;
  beforeEach(() => {
    SearchFormComp = createMountSearchForm();
  });

  it("renders a <SearchForm /> html element", () => {
    expect(SearchFormComp.exists()).toBe(true);
  });

  it("renders correct SearchForm component props", () => {
    expect(SearchFormComp.find("StyledSearchForm").exists()).toBe(true);
    expect(SearchFormComp.find("StyledSearchInput").prop("name")).toEqual(
      "search"
    );
    expect(SearchFormComp.find("StyledSearchInput").prop("type")).toEqual(
      "text"
    );
    expect(
      SearchFormComp.find("StyledSearchInput").prop("defaultValue")
    ).toEqual("test");
    expect(SearchFormComp.find("StyledSearchButton").prop("type")).toEqual(
      "submit"
    );
    expect(SearchFormComp.find("StyledSearchButton").prop("value")).toEqual(
      "Search"
    );
  });

  it("onSubmit dispatch search action", () => {
    SearchFormComp.find("StyledSearchForm").simulate("submit", {
      preventDefault: () => {},
      target: {
        search: {
          value: "new value"
        }
      }
    });
    const actions = store.getActions();
    expect(actions).toEqual([
      { searchQuery: "new value", type: "ADD_SEARCH_QUERY" }
    ]);
  });
});

import React, { memo } from "react";
import { useDispatch } from "utils/reactReduxHooks";
import { addSearchQuery } from "actions/boardActions";
import PropTypes from "prop-types";
import {
  StyledSearchForm,
  StyledSearchInput,
  StyledSearchButton
} from "components/SearchForm/SearchForm.styled";

const SearchForm = ({ searchQuery }) => {
  const dispatch = useDispatch();

  const handleSubmit = event => {
    event.preventDefault();
    dispatch(addSearchQuery(event.target.search.value));
  };

  return (
    <StyledSearchForm onSubmit={handleSubmit}>
      <StyledSearchInput name="search" type="text" defaultValue={searchQuery} />
      <StyledSearchButton type="submit" value="Search" />
    </StyledSearchForm>
  );
};

SearchForm.propTypes = {
  searchQuery: PropTypes.string
};

export default memo(SearchForm);

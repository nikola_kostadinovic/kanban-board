import React, { memo } from "react";
import PropTypes from "prop-types";
import {
  StyledEditButton,
  StyledCloseTaskEdition,
  StyledEditTask
} from "components/TaskForm/TaskForm.styled";

const TaskForm = ({
  onCloseClick,
  submitButtonValue,
  placeholder,
  onSubmit,
  defaultValue
}) => {
  const handleSubmit = event => {
    event.preventDefault();
    const newTask = event.target.task.value;
    newTask && onSubmit(newTask);
  };

  return (
    <StyledEditTask onSubmit={handleSubmit}>
      <textarea
        row="4"
        name="task"
        type="text"
        placeholder={placeholder}
        defaultValue={defaultValue || ""}
      />
      <StyledEditButton type="submit" value={submitButtonValue || "Submit"} />
      <StyledCloseTaskEdition onClick={onCloseClick}>X</StyledCloseTaskEdition>
    </StyledEditTask>
  );
};

TaskForm.propTypes = {
  placeholder: PropTypes.string,
  submitButtonValue: PropTypes.string,
  defaultValue: PropTypes.any,
  onSubmit: PropTypes.func,
  onCloseClick: PropTypes.func
};

export default memo(TaskForm);

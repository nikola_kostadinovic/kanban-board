import React from "react";
import { shallow } from "enzyme";
import TaskForm from "components/TaskForm/TaskForm";

const taskFormProps = {
  placeholder: "test placeholder",
  submitButtonValue: "test",
  defaultValue: "defValue",
  onSubmit: () => {},
  onCloseClick: () => {}
};
function createShallowTaskForm(props) {
  return shallow(<TaskForm {...taskFormProps} {...props} />);
}

describe("<TaskForm />", () => {
  let TaskFormComp;
  beforeEach(() => {
    TaskFormComp = createShallowTaskForm();
  });

  it("renders a <TaskForm /> html element", () => {
    expect(TaskFormComp.exists()).toBe(true);
  });

  it("renders correct taskForm component props", () => {
    expect(TaskFormComp.find("textarea").prop("row")).toEqual("4");
    expect(TaskFormComp.find("textarea").prop("type")).toEqual("text");
    expect(TaskFormComp.find("textarea").prop("placeholder")).toEqual(
      "test placeholder"
    );
    expect(TaskFormComp.find("textarea").prop("defaultValue")).toEqual(
      "defValue"
    );
    expect(TaskFormComp.find("StyledEditButton").prop("type")).toEqual(
      "submit"
    );
    expect(TaskFormComp.find("StyledEditButton").prop("value")).toEqual("test");
    expect(TaskFormComp.find("StyledCloseTaskEdition").exists()).toBe(true);
  });

  it("Calls onCloseClick ", () => {
    const closeClikSpy = jest.spyOn(taskFormProps, "onCloseClick");
    TaskFormComp = createShallowTaskForm({ onCloseClick: closeClikSpy });
    TaskFormComp.find("StyledCloseTaskEdition").simulate("click");
    expect(closeClikSpy).toHaveBeenCalledWith();
    closeClikSpy.mockRestore();
  });

  it("Call handleSubmit and calls onSubmit with new field", () => {
    const onSubmitSpy = jest.spyOn(taskFormProps, "onSubmit");
    TaskFormComp = createShallowTaskForm({ onSubmit: onSubmitSpy });

    TaskFormComp.find("StyledEditTask").simulate("submit", {
      preventDefault: () => {},
      target: {
        task: {
          value: "new value"
        }
      }
    });
    expect(onSubmitSpy).toHaveBeenCalledWith("new value");
    onSubmitSpy.mockRestore();
  });

  it("Call handleSubmit with empty state.field value, onSubmit is not called", () => {
    const onSubmitSpy = jest.spyOn(taskFormProps, "onSubmit");
    TaskFormComp.find("StyledEditTask").simulate("submit", {
      preventDefault: () => {},
      target: {
        task: {
          value: ""
        }
      }
    });
    expect(onSubmitSpy).not.toHaveBeenCalled();
    onSubmitSpy.mockRestore();
  });
});

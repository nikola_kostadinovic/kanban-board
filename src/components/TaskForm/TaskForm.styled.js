import styled from "styled-components";

export const StyledEditButton = styled.input`
  display: block;
  color: white;
  font-size: 16px;
  padding: 0;
  font-weight: bold;
  background: transparent;
  border: transparent;

  :hover {
    cursor: pointer;
  }
`;
StyledEditButton.displayName = "StyledEditButton";

export const StyledCloseTaskEdition = styled.span`
  position: absolute;
  top: 0px;
  right: 0px;
  color: white;
  font-weight: bold;

  :hover {
    cursor: pointer;
  }
`;
StyledCloseTaskEdition.displayName = "StyledCloseTaskEdition";

export const StyledEditTask = styled.form`
  position: relative;

  textarea {
    width: calc(100% - 30px);
  }
`;
StyledEditTask.displayName = "StyledEditTask";

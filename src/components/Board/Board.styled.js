import styled from "styled-components";

export const StyledBoard = styled.div`
  font-family: "Open sans";
`;
StyledBoard.displayName = "StyledBoard";

export const StyledBoardLists = styled.div`
  display: flex;
  flex-direction: row;
  font-family: "Open sans";
`;
StyledBoardLists.displayName = "StyledBoardLists";

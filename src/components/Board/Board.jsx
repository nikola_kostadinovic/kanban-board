import React, { memo } from "react";
import { useSelector } from "utils/reactReduxHooks";
import List from "components/List/List";
import SearchForm from "components/SearchForm/SearchForm";
import { StyledBoard, StyledBoardLists } from "components/Board/Board.styled";

const Board = () => {
  const tasks = useSelector(state => state.boardReducer.tasks);
  const searchQuery = useSelector(state => state.boardReducer.searchQuery);

  const getTaskList = listStatus => {
    if (searchQuery) {
      return tasks.filter(
        task => task.status === listStatus && task.title.includes(searchQuery)
      );
    }
    return tasks.filter(task => task.status === listStatus);
  };

  const statusLists = [
    { listStatus: "to-do" },
    { listStatus: "in-progress" },
    { listStatus: "done" }
  ];

  return (
    <StyledBoard>
      <SearchForm searchQuery={searchQuery} />
      <StyledBoardLists>
        {statusLists.map(list => (
          <List
            key={list.listStatus}
            listStatus={list.listStatus}
            taskList={getTaskList(list.listStatus)}
          />
        ))}
      </StyledBoardLists>
    </StyledBoard>
  );
};

export default memo(Board);

import React from "react";
import { mount } from "enzyme";
import configureMockStore from "redux-mock-store";
import thunkMiddleware from "redux-thunk";
import { Provider } from "react-redux";

import { mockTasks } from "reducers/boardReducer";
import Board from "components/Board/Board";

const mockStore = configureMockStore([thunkMiddleware]);
const initialState = {
  boardReducer: {
    tasks: [...mockTasks]
  }
};

function createMountBoard(overrideStore) {
  const store = mockStore(initialState);

  return mount(
    <Provider store={overrideStore || store}>
      <Board />
    </Provider>
  );
}

describe("<Board />", () => {
  let BoardComponent;

  beforeEach(() => {
    BoardComponent = createMountBoard();
  });

  it("renders a <Board /> html element", () => {
    expect(BoardComponent.exists()).toBe(true);
  });

  it("renders right number of Tasks, test for getTaskList", () => {
    expect(BoardComponent.exists()).toBe(true);
    expect(BoardComponent.find("Memo(List)")).toHaveLength(3);
    expect(
      BoardComponent.find("Memo(List)")
        .at(0)
        .find("Memo(Task)")
    ).toHaveLength(3);
    expect(
      BoardComponent.find("Memo(List)")
        .at(1)
        .find("Memo(Task)")
    ).toHaveLength(2);
    expect(
      BoardComponent.find("Memo(List)")
        .at(2)
        .find("Memo(Task)")
    ).toHaveLength(1);
  });

  it("renders tasks included in search query", () => {
    BoardComponent = createMountBoard(
      mockStore({
        boardReducer: {
          tasks: [...mockTasks],
          searchQuery: "Run"
        }
      })
    );
    expect(BoardComponent.exists()).toBe(true);
    expect(BoardComponent.find("Memo(List)")).toHaveLength(3);
    expect(
      BoardComponent.find("Memo(List)")
        .at(0)
        .find("Memo(Task)")
    ).toHaveLength(0);
    expect(
      BoardComponent.find("Memo(List)")
        .at(1)
        .find("Memo(Task)")
    ).toHaveLength(0);
    expect(
      BoardComponent.find("Memo(List)")
        .at(2)
        .find("Memo(Task)")
    ).toHaveLength(1);
  });
});

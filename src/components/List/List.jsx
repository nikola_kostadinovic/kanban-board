import React, { memo, useState, useCallback } from "react";
import { useDispatch } from "utils/reactReduxHooks";
import PropTypes from "prop-types";
import Task from "components/Task/Task";
import TaskForm from "components/TaskForm/TaskForm";
import { createTask, moveTask } from "actions/boardActions";

import {
  StyledList,
  StyledListHeader,
  StyledListTitle,
  StyledTaskList,
  StyledAddNewTask,
  StyledCreateTask
} from "components/List/List.styled";

const List = ({ listStatus, taskList }) => {
  const [openCreateTask, setOpenCreateTask] = useState(false);
  const dispatch = useDispatch();

  const onSubmit = newTitle => {
    newTitle && dispatch(createTask({ title: newTitle, status: listStatus }));
    toggleNewTask();
  };

  const toggleNewTask = useCallback(() => {
    setOpenCreateTask(prevState => !prevState);
  }, []);

  const openNewTask = useCallback(() => {
    setOpenCreateTask(true);
  }, []);

  const onDragOver = event => {
    event.preventDefault();
  };

  const onDrop = useCallback(
    (event, moveTo) => {
      dispatch(moveTask(event.dataTransfer.getData("text/plain"), moveTo));
    },
    [moveTask],
  );


  return (
    <StyledList>
      <StyledListHeader listStatus={listStatus}>
        <StyledListTitle> {listStatus.replace("-", " ")} </StyledListTitle>
        <StyledAddNewTask onClick={openNewTask}>
          +
        </StyledAddNewTask>
        <div> ({taskList.length})</div>
      </StyledListHeader>
      <StyledTaskList
        listStatus={listStatus}
        onDrop={event => onDrop(event, listStatus)}
        onDragOver={event => onDragOver(event)}
      >
        {taskList.map(task => (
          <Task key={task.id} {...task} />
        ))}
        {openCreateTask && (
          <StyledCreateTask listStatus={listStatus}>
            <TaskForm
              placeholder="Enter title for new task"
              onSubmit={onSubmit}
              onCloseClick={toggleNewTask}
              defaultValue={""}
              submitButtonValue="Create"
            />
          </StyledCreateTask>
        )}
      </StyledTaskList>
    </StyledList>
  );
};

List.propTypes = {
  listStatus: PropTypes.string.isRequired,
  taskList: PropTypes.array.isRequired
};

export default memo(List);

import React from "react";
import { mount } from "enzyme";
import List from "components/List/List";
import configureMockStore from "redux-mock-store";
import thunkMiddleware from "redux-thunk";
import { Provider } from "react-redux";
import { mockTasks } from "reducers/boardReducer";

jest.mock("uuid/v1", () => () => "00000000-0000-0000-0000-000000000000");

const mockStore = configureMockStore([thunkMiddleware]);
const initialState = {
  boardReducer: {
    tasks: [...mockTasks]
  }
};

const listProps = {
  listStatus: "to-do",
  taskList: [
    { id: 123, title: "mocked task 1", status: "to-do" },
    { id: 223, title: "mocked task 2", status: "to-do" }
  ]
};

function createMountList(store, props) {
  return mount(
    <Provider store={store}>
      <List {...listProps} {...props} />
    </Provider>
  );
}

describe("<List />", () => {
  let ListComp;
  let store;
  beforeEach(() => {
    store = mockStore(initialState);
    ListComp = createMountList(store);
  });

  it("renders a <List /> html element", () => {
    expect(ListComp.exists()).toBe(true);
  });

  it("renders correct List component props and elements", () => {
    expect(ListComp.find("StyledListHeader").prop("listStatus")).toEqual(
      "to-do"
    );
    expect(ListComp.find("StyledListTitle").text()).toEqual(" to do ");
    expect(ListComp.find("StyledAddNewTask").exists()).toBe(true);
    expect(
      ListComp.find("div")
        .at(1)
        .text()
    ).toEqual(" (2)");
    expect(ListComp.find("StyledTaskList").prop("listStatus")).toEqual("to-do");
    expect(
      ListComp.find("Memo(Task)")
        .at(0)
        .prop("id")
    ).toEqual(123);
    expect(
      ListComp.find("Memo(Task)")
        .at(1)
        .prop("id")
    ).toEqual(223);
  });

  it("Change state openCreateTask to true on Plus button click", () => {
    ListComp.find("StyledAddNewTask").simulate("click");
    expect(ListComp.find("StyledCreateTask").exists()).toBe(true);
  });

  it("Dispatch createTask action on TaskForm submit, and toggleNewTask TaskForm", () => {
    ListComp.find("StyledAddNewTask").simulate("click");
    expect(ListComp.find("StyledCreateTask").exists()).toBe(true);
    ListComp.find("form").simulate("submit", {
      preventDefault: () => {},
      target: {
        task: {
          value: "new value"
        }
      }
    });
    const actions = store.getActions();
    expect(actions).toEqual([
      {
        newTask: {
          id: "00000000-0000-0000-0000-000000000000",
          status: "to-do",
          title: "new value"
        },
        type: "CREATE_TASK"
      }
    ]);
    expect(ListComp.find("StyledCreateTask").exists()).toBe(false);
  });

  it("Call onDragOver and prevent event", () => {
    const mockEvent = {
      preventDefault: () => {}
    };

    const eventSpy = jest.spyOn(mockEvent, "preventDefault");
    ListComp.find("StyledTaskList").simulate("dragover", mockEvent);
    expect(eventSpy).toHaveBeenCalled();
    eventSpy.mockRestore();
  });

  it("Call onDrop, prevent event, get id with getData from dataTransfer and dispatch moveTask", () => {
    ListComp.find("StyledTaskList").simulate("drop", {
      preventDefault: () => {},
      dataTransfer: {
        getData: () => {
          return 123;
        }
      }
    });

    const actions = store.getActions();
    expect(actions).toEqual([
      {
        id: 123,
        status: "to-do",
        type: "MOVE_TASK"
      }
    ]);
  });
});

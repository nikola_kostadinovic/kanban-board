import styled from "styled-components";
import * as colors from "style/colors";
import { bounce } from "style/animations";

export const StyledList = styled.div`
  width: 200px;
  height: 80vh;
  border: 2px solid white;
  flex: 1;
`;
StyledList.displayName = "StyledList";

export const StyledListHeader = styled.header`
  position: relative;
  background-color: ${props =>
    props.listStatus === "to-do"
      ? colors.blue
      : props.listStatus === "in-progress"
      ? colors.pink
      : colors.black};
  color: white;
  height: 80px;
  padding: 10px 0px;
  border-bottom: 2px solid white;
  text-align: center;
  font-size: 20px;
  font-weight: bold;
`;
StyledListHeader.displayName = "StyledListHeader";

export const StyledListTitle = styled.h3`
  margin: 0;
  text-transform: capitalize;
`;
StyledListTitle.displayName = "StyledListTitle";

export const StyledTaskList = styled.div`
  background-color: ${props =>
    props.listStatus === "to-do"
      ? colors.lighterBlue
      : props.listStatus === "in-progress"
      ? colors.lighterPink
      : colors.lighterBlack};
  padding: 10px 30px;
  height: 100%;
`;
StyledTaskList.displayName = "StyledTaskList";

export const StyledAddNewTask = styled.span`
  position: absolute;
  top: 30px;
  right: 10px;
  color: white;
  font-weight: bold;
  font-size: 30px;

  :hover {
    cursor: pointer;
  }
`;
StyledAddNewTask.displayName = "StyledAddNewTask";

export const StyledCreateTask = styled.div`
    align-self: flex-end;
    animation-duration: 2s;
    animation-iteration-count: once;
    transform-origin: bottom;
    animation-name: ${bounce};
    animation-timing-function: ease;
    padding: 10px 15px;
    margin-top: 5px;
    color: white
    background-color: ${props =>
      props.listStatus === "to-do"
        ? colors.lightBlue
        : props.listStatus === "in-progress"
        ? colors.lightPink
        : colors.lightBlack};`;
StyledCreateTask.displayName = "StyledCreateTask";

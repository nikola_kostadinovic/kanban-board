import React from "react";
import { mount } from "enzyme";
import Task from "components/Task/Task";
import configureMockStore from "redux-mock-store";
import thunkMiddleware from "redux-thunk";
import { Provider } from "react-redux";
import { mockTasks } from "reducers/boardReducer";

const mockStore = configureMockStore([thunkMiddleware]);
const initialState = {
  boardReducer: {
    tasks: [...mockTasks]
  }
};

const taskProps = {
  id: 123,
  title: "test title",
  status: "to-do"
};

function createMountTask(store, props) {
  return mount(
    <Provider store={store}>
      <Task {...taskProps} {...props} />
    </Provider>
  );
}

describe("<Task />", () => {
  let TaskComp;
  let store;
  beforeEach(() => {
    store = mockStore(initialState);
    TaskComp = createMountTask(store);
  });

  it("renders a <Task /> html element", () => {
    expect(TaskComp.exists()).toBe(true);
  });

  it("renders correct task component props", () => {
    expect(TaskComp.find("StyledTask").prop("draggable")).toEqual("true");
    expect(TaskComp.find("StyledTask").prop("status")).toEqual("to-do");
    expect(TaskComp.find("StyledTaskTitle").text()).toEqual("test title ");
    expect(TaskComp.find("StyledCloseTask").exists()).toBe(true);
  });

  it("renders correct task component and TaskForm when isTaskEditable is true from duble click", () => {
    TaskComp.find("StyledTask").simulate("dblclick");
    expect(TaskComp.find("StyledTask").prop("draggable")).toEqual("true");
    expect(TaskComp.find("StyledTask").prop("status")).toEqual("to-do");
    expect(TaskComp.find("StyledTaskTitle").exists()).toBe(false);
    expect(TaskComp.find("StyledCloseTask").exists()).toBe(false);
    expect(TaskComp.find("Memo(TaskForm)").exists()).toBe(true);
  });

  it("Dispatch delete action on button click", () => {
    TaskComp.find("StyledCloseTask").simulate("click");
    const actions = store.getActions();
    expect(actions).toEqual([{ id: 123, type: "DELETE_TASK" }]);
  });

  it("Dispatch editTask action on TaskForm submit, and close TaskForm", () => {
    TaskComp.find("StyledTask").simulate("dblclick");
    expect(TaskComp.find("Memo(TaskForm)").exists()).toBe(true);
    TaskComp.find("form").simulate("submit", {
      preventDefault: () => {},
      target: {
        task: {
          value: "new value"
        }
      }
    });
    const actions = store.getActions();
    expect(actions).toEqual([
      { id: 123, title: "new value", type: "EDIT_TASK" }
    ]);
    expect(TaskComp.find("Memo(TaskForm)").exists()).toBe(false);
  });

  it("Did not dispatch editTask action on TaskForm submit because new title is same as old, and close TaskForm", () => {
    TaskComp.find("StyledTask").simulate("dblclick");
    expect(TaskComp.find("Memo(TaskForm)").exists()).toBe(true);
    TaskComp.find("form").simulate("submit", {
      preventDefault: () => {},
      target: {
        task: {
          value: "test title"
        }
      }
    });
    const actions = store.getActions();
    expect(actions).toEqual([]);
    expect(TaskComp.find("Memo(TaskForm)").exists()).toBe(false);
  });

  it("On call onDragStart, setData is executed", () => {
    const mockEvent = {
      dataTransfer: {
        setData: () => {}
      }
    };

    const eventSpy = jest.spyOn(mockEvent.dataTransfer, "setData");
    TaskComp.find("StyledTask").simulate("dragstart", mockEvent);
    expect(eventSpy).toHaveBeenCalledWith("text/plain", 123);
    eventSpy.mockRestore();
  });
});

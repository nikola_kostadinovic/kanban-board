import React, { memo, useState } from "react";
import { useDispatch } from "utils/reactReduxHooks";
import PropTypes from "prop-types";
import TaskForm from "components/TaskForm/TaskForm";
import { editTask, deleteTask } from "actions/boardActions";
import {
  StyledTask,
  StyledCloseTask,
  StyledTaskTitle
} from "components/Task/Task.styled";

const Task = ({ id, title, status }) => {
  const [isTaskEditable, setIsTaskEditable] = useState(false);
  const dispatch = useDispatch();

  const closeTaskEdit = () => {
    setIsTaskEditable(false);
  };

  const onSubmit = newFieldValue => {
    newFieldValue !== title && dispatch(editTask(id, newFieldValue));
    closeTaskEdit();
  };

  const onDragStart = (event, id) => {
    event.dataTransfer.setData("text/plain", id);
  };

  return (
    <StyledTask
      status={status}
      draggable="true"
      onDragStart={event => onDragStart(event, id)}
      onDoubleClick={() => setIsTaskEditable(true)}
    >
      {isTaskEditable ? (
        <TaskForm
          onSubmit={onSubmit}
          onCloseClick={closeTaskEdit}
          defaultValue={title}
          submitButtonValue="Update"
        />
      ) : (
        <div>
          <StyledTaskTitle>{title} </StyledTaskTitle>
          <StyledCloseTask onClick={() => dispatch(deleteTask(id))}>
            X
          </StyledCloseTask>
        </div>
      )}
    </StyledTask>
  );
};

Task.propTypes = {
  id: PropTypes.any.isRequired,
  title: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired
};

export default memo(Task);

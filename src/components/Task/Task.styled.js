import styled from "styled-components";
import { lightBlue, lightPink, lightBlack } from "style/colors";
import { bounce } from "style/animations";

export const StyledTask = styled.div`
    position: relative;
    padding: 10px 15px;
    margin-top: 5px;
    color: white
    background-color: ${props =>
      props.status === "to-do"
        ? lightBlue
        : props.status === "in-progress"
        ? lightPink
        : lightBlack};
    align-self: flex-end;
    animation-duration: 2s;
    animation-iteration-count: once;
    transform-origin: bottom;
    animation-name: ${bounce};
    animation-timing-function: ease;

    :hover{
        span{
            display:block;
        }
    }
    `;
StyledTask.displayName = "StyledTask";

export const StyledCloseTask = styled.span`
  position: absolute;
  display: none;
  padding: 5px;
  top: 0;
  right: 0;
  color: white;
  font-weight: bold;

  :hover {
    cursor: pointer;
  }
`;
StyledCloseTask.displayName = "StyledCloseTask";

export const StyledTaskTitle = styled.p`
  text-align: center;
  font-size: 16px;
  font-weight: bold;
  color: white;
`;
StyledTaskTitle.displayName = "StyledTaskTitle";

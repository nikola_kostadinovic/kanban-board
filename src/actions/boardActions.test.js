import { ActionTypes } from "./ActionTypes";
import * as actions from "./BoardActions";

jest.mock("uuid/v1", () => () => "00000000-0000-0000-0000-000000000000");

describe("Board Actions", () => {
  it("should create an action to create new task", () => {
    const payload = { title: "test title", status: "to-do" };
    const expectedAction = {
      type: ActionTypes.CREATE_TASK,
      newTask: {
        id: "00000000-0000-0000-0000-000000000000",
        title: "test title",
        status: "to-do"
      }
    };
    expect(actions.createTask(payload)).toEqual(expectedAction);
  });

  it("should create an action to move task", () => {
    const expectedAction = {
      type: ActionTypes.MOVE_TASK,
      id: 123,
      status: "done"
    };
    expect(actions.moveTask(123, "done")).toEqual(expectedAction);
  });

  it("should create an action to edit task", () => {
    const expectedAction = {
      type: ActionTypes.EDIT_TASK,
      id: 123,
      title: "test title"
    };
    expect(actions.editTask(123, "test title")).toEqual(expectedAction);
  });

  it("should create an action to delete task", () => {
    const expectedAction = { type: ActionTypes.DELETE_TASK, id: 123 };
    expect(actions.deleteTask(123)).toEqual(expectedAction);
  });

  it("should create an action to add search query", () => {
    const expectedAction = {
      type: ActionTypes.ADD_SEARCH_QUERY,
      searchQuery: "new task"
    };
    expect(actions.addSearchQuery("new task")).toEqual(expectedAction);
  });
});

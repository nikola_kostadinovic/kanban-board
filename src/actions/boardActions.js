import { ActionTypes } from "./actionTypes";
import uuidv1 from "uuid/v1";

export const createTask = ({ title, status }) => ({
  type: ActionTypes.CREATE_TASK,
  newTask: {
    id: uuidv1(),
    title,
    status
  }
});

export const moveTask = (id, status) => ({
  type: ActionTypes.MOVE_TASK,
  id,
  status
});

export const editTask = (id, title) => ({
  type: ActionTypes.EDIT_TASK,
  id,
  title
});

export const deleteTask = id => ({
  type: ActionTypes.DELETE_TASK,
  id
});

export const addSearchQuery = searchQuery => ({
  type: ActionTypes.ADD_SEARCH_QUERY,
  searchQuery
});

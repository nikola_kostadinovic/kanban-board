import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import Board from "components/Board/Board";
import { store, persistor } from "./store/configureStore";
import { PersistGate } from "redux-persist/integration/react";

render(
  <Provider store={store}>
    <PersistGate loading={"Loading tasks..."} persistor={persistor}>
      <Board />
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// import * as serviceWorker from './serviceWorker';
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

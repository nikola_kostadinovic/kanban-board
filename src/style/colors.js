
export const blue = '#1891da';
export const lightBlue = '#53b2ea';
export const lighterBlue = '#bee3f6';

export const pink = '#e12a5a';
export const lightPink = '#e56d77';
export const lighterPink = '#f4c2c5';

export const black = '#112442';
export const lightBlack = '#4c5e74';
export const lighterBlack = '#bbc2ca';

/**
 * We use this file, to be able to mock and test hooks in tests
 */

import {
  useSelector as originalUseSelector,
  useDispatch as originalUseDispatch
} from "react-redux";

export const useSelector = state => originalUseSelector(state);
export const useDispatch = () => originalUseDispatch();
